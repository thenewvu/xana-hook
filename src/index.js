'use strict';

const async = require('neo-async');

/**
 * Validate a function for hooking.
 * @param {Function} func
 * @throws {TypeError}
 * @private
 */
function validateFunc(func) {
  if (typeof func != 'function') {
    throw new TypeError(`hooking expects a function but got ${func}`);
  }
}

/**
 * Create a proxy handler for hooking.
 * @returns {Proxy.handler}
 * @private
 */
function createHookingHandler() {
  const prevs = [];
  const posts = [];
  const fails = [];
  const utils = {
    prev(middleware) {
      validateFunc(middleware);
      prevs.push(middleware);
    },
    post(middleware) {
      validateFunc(middleware);
      posts.push(middleware);
    },
    fail(middleware) {
      validateFunc(middleware);
      fails.push(middleware);
    }
  };

  return {
    get(target, property) {
      if (utils.hasOwnProperty(property)) {
        return utils[property];
      } else {
        return target[property];
      }
    },
    apply(target, context, args) {
      const done = args.pop();
      async.waterfall(
        [(next) => next(null, ...args), ...prevs, target.bind(context), ...posts],
        (err, ...res) => (!err) ? done(null, ...res) : async.waterfall(
          [(next) => next(null, err), ...fails],
          (err1, err2) => done(err1 || err2)
        )
      );
    }
  };
}

/**
 * Hook a function.
 * @param {Function} func
 * @returns {Proxy} Hooked-version of the method.
 */
function hook(func) {
  validateFunc(func);
  return new Proxy(func, createHookingHandler());
}

module.exports = hook;
