'use strict';

const path    = require('path');
const chai    = require('chai');
const fs      = require('fs');
const yaml    = require('js-yaml');
const decache = require('decache');
const expect  = chai.expect;

function runTests(tests) {
  Object.keys(tests).forEach((describeName) => {
    describe(describeName, function () {
      Object.keys(tests[describeName]).forEach((itName) => {
        it(itName, function (done) {
          const itData = tests[describeName][itName];
          decache('../src/index.js');
          const hook   = require('../src/index.js');
          const hooked = hook(itData.func);
          itData.prevs && itData.prevs.forEach(hooked.prev);
          itData.posts && itData.posts.forEach(hooked.post);
          itData.fails && itData.fails.forEach(hooked.fail);
          hooked(... itData.args, (err, ...res) => {
            expect(err).to.deep.equal(itData.err);
            expect(res).to.deep.equal(itData.res);
            done();
          });
        });
      });
    });
  });
}

const testDir   = path.dirname(__filename);
const testFiles = fs.readdirSync(testDir);
testFiles.forEach(filename => {
  if (filename.endsWith('.yaml')) {
    const filepath  = path.join(testDir, filename);
    const testData  = fs.readFileSync(filepath);
    const testCases = yaml.load(testData);
    runTests(testCases);
  }
});

